// ==UserScript==
// @name         BooruTags
// @version      1.9.210124
// @description  Копирование тегов с сайтов
// @author       REIONE
// @run-at       document-end
// @grant        GM.xmlHttpRequest

// @icon         data:image/x-icon;base64,iVBORw0KGgoAAAANSUhEUgAAAEAAAABACAIAAAAlC+aJAAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAAABmJLR0QAAAAAAAD5Q7t/AAAAB3RJTUUH5QgREAIFDPJu1wAAH8VJREFUaN6deneYXMWVb52qurH7dvf05CxpRllCEgpkISGCWEBgAwb8jL0Yv8X24rAY7+OxNuvFYO8uDp+Xtd86P9s4wmJMlJCFCBJCOUuj0eQ80zOdb66wf/TMaBQw+159833T997qvr+TT51z4KG5S9FfXICQRAjef4NESMO4K++MLV3pHD9+tSEZYEBSIkQBAKFQSirllqIclXSTFsR1hQI6lA922OLupFKmEiblOa8ofdcO2dF8sKLMUAAJNIVjeqtEEiH8F3DD1E4498lZl4BAYqz43sLG2svvufPIcNpUiJBIARj1Wa8bqhh7XPhEUQzd4QIDcC6GOI5EIlwIAIQQ0jDgGT8NAIixk9gYK6sMgxAAQCKAC4DEFwT9/ohLHJjEXeIDIMmFSEaMrkNHP3bf3enquqLjYYw1DKcKwcGsbxBwmZCKoqqqzYWCIR+yoqqbqoKkRAgAoYM53+USAwBCUiKT4MNZt279+hbCIxTzKd4DmkEGIDSTgPdXEjmDOgkIAAEgiZCcIhWEkFFDz/T0ABd3Pvg3B8ZyBsVcyhAhRyIupCekpIqiKjaXCsCoxyESpSABIQKowMS7Ew6TCBASEpmqcnxwrOGWWxrKE8rQgKprkxjgzH+YAo3RBVh+vqRgSjIzyT9LWoRSK/D37j7wqU991G+alSk4ACAI9RTV48KXCFMFYywQGBhGmNRMA3FOAChAt8MIISYBLqVCSLZQHK5t/vSXPnv49TeaE9GACZgB5ZyF4QNwA3wAdZM/K6RMUjiy/1DCilx/9+1tGZsCCKrgSNRmosgEVpWIoY8zeTxjpxCNmAZwHqXAwrCb46QVwVIiAIrkjoz3Tz946vTxU/aptpgVEVLABfR/knn0bOWeuU/+d3BPb2VSNsQi217fPjSSuvMjm7b85BcsZBwBAHq2P8OoqkDKM3Vbi6apynVmENzlBi87YW3UdDS9ins6Bqwou3uHr3nggeuvueLWa++cF9NDIWEKDFwIFp2CLs/jNKC/fD3zEQCV8u3xQst1l7q2M79l1rIbrn35pz+SzfNXLF4Yu+oSK2o6RdvzfIJxJpMdHBw5daojFMIpK3frayqKdjY1fiRnNyue27rgK//wxTfe2TN26PDSiqjLxQVhTZNBp7XlA9n8fksiZGDY3D++7oufe+yRB33fdxwn5HzpJ+7/8sOfbmqs55wTQgCAc8EYy+cL+Xyhvb3rvff2Hz507MTxdsD45ts2ZsbTz/znH3/wraeSZbGnv/WD2RAiEgXBAIE8l/UlL4IkQvRCvP9/Qx+hZFvPyJUPfvbJx/4unclJKb/85cfXr7/inns+xEJ28NCxwcGhDRuu9jxPURQhRDxuJZOJefNabr75uuHh0bfe2vXDH/7yvR17btl0Q8NDn9t4/brR0XGqKj1O2DKlOCVfJ8/QIKfhkSvKay6I+wNdU2nplLSlsrEbNn7n21/LZnOWFf35z3+3Zs2KO+64JZWasG1n69Y3r7tuPcZYUZQTJ9pM09Q0NQhCzwt8349EIhdfvPSuu25LZzK/+90Ll1928WWXr2aM3XHHLe/2jpzcva+1zPI5ByiRUTLRswIBuby8+nzof1kgM8mjCO0qsi9987HZDbWE0j//+W0AdOedm0ZGxpLJsueee3Hhwrnz5rW4rgsABw4cwRjy+UJNTRXnHGMshHAcF2O8fv2VCxbM3blz7/Dw2Jo1KzzXu2Hj+l9vfpuMDCWjJps0ZYTO0DB5A/93WP7+aQWSQgKl7723f3hkzPf9trb2m266LpPJGoaeSk1s27ajpWW247iU0pIKSYlGRsYIoVJKhBAhRFVVz/PT6ezGjes/9rE7RkbGEIIgDCOa+sjXHznog+T8PDs+Qw9G/z3c77coxVXc37vv8MGDxzdvfmPp0kWRSCQMw0jEfOftXRTj8ookYwxjCEOmKEo0GimlCwghjHEul5dSDA0NHz58DCEYG0tt3LgeAGGMvSBoqK3SGpvSjkcxPg9aCZjE8P+Fe3qnz+Wyskjfrt279h16b9e+1tbZvu8TTDjne3bta21pxhgjiQCAMQaAVFXlnEuJhJC6rnV2dh871lZZWZHL5X0/WL58SaFQDIKQUurYzptvvefn86ZChZQXhATnZ6Mf6I9gOqMAJBESSMYM7Zoo/sV//NLxgsrKCt/3VVUZHUtNjI1XVlYIISSSALgECwAFQViK8JzziorydDobi1m27fi+T6mSzWb7+wcxxrv3HHrhhdeqC5mopgp5xu2ck2jSD4aOJhGXfkMiwACAEcGgALh+0JXKdBT9lZddsWrVMk1TAUBVlfaTHYHnI4RKug4AYVgiABjjpTuM8Xg8ViwWCSGO4zLG8vl8ZWVFZ2f38NDo8y++fvqdnZ+cU+mdseAzUM8PZO/DdEBITua8mkKwlGEQ+CELGCuGrMvlaTO2eO01i6urNUr+8Ns/RkztoosWX3TR4q6ePkPTKKUYY86FFMLzfCEEY5wxJoTkXCDEVFUtFOySRyoWbU3TDMPI5+2n//1nK1Ysmf+VL/35O0+vrytz+fvSQC+QMMgz6KVEGGODYMd1O8YKGUSUqiq9pjJeVRkpL79j5YrVq5fNndO8/Y0dv/rF71evumjNJSv37jn4218/39HRfejAMbMiGfh+LBaNRqOGoQeBGY1GDEO3rAhCklIKGCilZWXxiopkNptLJOL9/YONjXU1NRUPP/xZKxZN5wq7fvzTK5qq7JBdkAZ6LtvllKJLJCQyFVq0ncPpgmiatfKOez66YW1ZMoERmjt3jqZQK2J4rn/g4NF9+w71Dww99PBnliyen8vmD727b9m8FuDi5Ze2BkFgRsxZTfX5fJEopKmxvrtnoLKyIpPJFvKFTCa//c0dhw6f6OsbePKJRxBChw8ff/DB+9evv6JYtAPff/xrD3/8VEfHnp2zK8t8xs/nNj3nelqZJKAIJceGUuM1jR/7t39aednqoZ7eP/32+Xf3Hv7CFz+1sHVW4Hmvvf3e9u07bdseHU098Y1HFy6cl8sXe3v6Orr7+odGAaBlVuNbb+9ubZk12NM/u77W9f3tW96c09z4b3sPeUEQcG47LhfyxLFT8+fNqa2t5pz3dPX09Q20ts7u7x+YP38uAXjoKw89+qEDszg/R/tLC09jh2kLlwghpBO8vXes+p7/8bNtfwSMH//EZ76y8cNbX9n69W8+eu/H77Id95lnntuz58CmTTe0ts6+7757OGNhGBKMWcg6eweWr1oer0juO3isq6vnjZ37hG4U/CASMV0/LAbhnIVza5sasKJiQsusKMU4CJmUEgG0NjccPnCkqbkxny+oqnL02EnL1I3GxoLrEXyBqEUnb00fGxGSCJmU/LlndONXH73rntu+9Mkv+Lt2loGoWrPmmV/9YEHLrLZTHbt377/00pVr1lz84oubFy2a+/LLWzdu3BCJmEIIQrDv+ddfv/bmTRs/eePdemfnuMTHN297jcuPfWjjkgWtR051dvcN7jt4HHEWUVWqay2ts6mqSCEQQDxmubatKArnQko5Nja+98BRoAqXZ+dAU5DxOUcdKZGp0n39o6v+5lMbrlv70atuqj6yd35ZpHfW/Kd++YM5jfUn2zr27Nm/bt2VK1ZcdOJE++hoyjTNnp6+RYvm+n4AgBHg1SsWP/n1727bvmPppSvDfOEf5lQtiZmXXrFaoWRB62xNUdpOnL7a0j8zr/E6k95104YXXvzFhmuuKAUKIZEfhKV4l88Xll20eHBotJhKmSqdjgbnqdAMGijB6VzBn794/V9d+9BtH1tHgphpvAWRb//831oaaodHx0roa2urR0ZGf//7Fy65ZOWLL25JJBKcC4SQlCIaNT/913etXLpg//YdR0532ytX/izA+xkaGxxevmTBhrVrll00f+GClkvLYysp2EJefcO6X//6udc2v5koixOCc4UiEGLoGgBybLe+viZkHA0PRg1dSDkT6nQ2ecYnSYlUgFMOu+quD//4q0+uEo6hR/804f3L7364ZO5s23F3vbtn1arl9fV1b7zxzne/+8Orr75sdDSVTmfHx9MDA8PLlycBQCB0or3ziUc/L4R44533brlxw3A6m//G91YuXfDhm6+byGQbK8tjV66ORIy+ohPsOfh/f/Vcemz8I3ffyrhQCD3Z0X377TdjDEIILjhgXJVMFBUoFVYmjzYzfD2dSRHFeCJfVBcvPbR7f7T9ZH1j1XPdqc99/9tXX7bSdtz29g7LspYsWfijH/3ylVe2YYyvv37d5s3bw5A1Ntbv3Lln9erlYRhalnW6u68sHhseG7/x2rWFXKExmaipKCeEUoo1Vbn3rlvT6awXBJWVFRuuW6tS+srWt6qqKxSFOrazdOnC5cuXFotOGDJCCCDU0jq7HWEkL3xGx+eII4+wbZgn3tm1sCa5s3f0svs/8fG7Nk2kM5zzkyfbr7zykt/85vnXX39r9uwmVVUBoLe3nzEWiZiDg8NDQyNIoubmhu7BkfbO7kTcKtqOoipEofPnzu4bHEEIuBCeFyQS8eaGOhYE1RXlbZ09G6+5ElOqUCqlrKwsp5TatoMxrqgon5jItLV1YjzlaM4+0UwScOamlIIqb7y1p4qHPGTjVXWf+/yncrm8aZodHd01NVXj4+mXX369rq4GAIQQlFLfD3Vdy2Rytu12dHQpqqJQYiUSX/v2j0ZGUslEfHg01T84cu+dt9x16/VhGGqUKpRIIf746p/bTnfFYtFbrl/39E9/i4AQgjkXPT0DqqoUCgVKaWdHzx//tHnn9neqTS0UkmAgAGQqgS65n7NTCQDGBfLdlrrK7kzukns/3FBbNZ5KJxJGZ2f3qlUrXnjhNc5FNBopFGzLioZhKKWUUqqqMjGR2b374KWXropEIlbUnJjIPPbt//PYQw+oqvKz3zxfWZ6sqEgOT2S8IOzs7W87cXrTDeuuvGTl0MjoL559uXNwtKGhlnMRBMHY2Hg+X9i27Z18rjA6nHr+1TeU3u7G+kTIhRuEAWAAiBKsYgiERGen0wAICSQRYItAVsKK1SswAkJIJpMZHBxRVeX48VOWFS0WbdM0PM/LZLKUEoSQqiqxWPTIkRN9fYPRaMSyolbEpLpxz30PHTna9sgXPpUvFH//py1HO3o6B0ZMTfvKQw9cu/bSZ196/X994+n2/uHmxrqamirGGGNcCPGHP7x47OhJKxF/4bXt6bffvqYm5nJxyGY9sYoJPdrthC+OFNvtQMWApupCJcsGLmWlSi1KACGK8Xu7D6y7YnWyvOzgwWNSoomJdCaTa2yst23HsqKe5x8/3h6NRtraOmKxqJTo6NG2rVvfWrZskWma27btYAjWIPbNf3zqxe3vPvnlz/zvL/zPgcFhJqSuq3sPHH3trffsIJSENDXWDQ+PVlWVA0BnR5dddAaGR12OfvXjZ+J9XTdWxxDGY0U3n6xaO6s2MzTaahAxMN5eDBZGVXlOXSiUqEIlS2OazcXsqL75la0ts5uuvPTidDojBO/s7E2nMw0Ndb4fPPvsS2HINE2rqqro6OiJx6wgDIeGxvbuPVSKjg7jLQpenoyZAR8YHFl32ydv3bh+1cqle3fuaevsi9TVNNdV83xBtSK7du276abrVE3DgP7w/Kt7D5+gxWKlnbuYivraModxRcqQ86VNtXOb6rttm0+Mz42qrBiUisH0HIsOhLy8zEAIWYq2cGL8mV/8IfD8skSsv3+4o6NHVVXbtjOZXBiw5qb6gf6hYrEYMY1kWYJxHgRhX9+Q43g+Y8T3Lykv9xEqC3wtalx77x2/+u0LP37mN2sjsdmxSLeUe0dTNRXJeMJKJOKLFs0zde3AkZP7t7yxiLuNhlJWpjMAO+QACACkRIAxyudyJ06YVlQiYFNO9dx0WiKkYgCEHMZXVsazPaef/vef3/fxO1pbZm3e8oaqKplMLpvNKaqCCVZVtb29m3Oh61o0GlUVJZ8vcM5TqTQNQwIQchGPGEcOHuvI2Y0NtQ5jqoJro1pXV3cv0KGB4WjEmD+/Zc3qZU4QPvHVf14VFhZUlxdD5gpZOrBP1+QkAMIEaxpgjIGX2jOT5fXzaRAIEYy9MARKKYHdew50dvVGIpGhoVEpZS5X0DWVc4ExlJcnKyuSruv7vu96XiRiplITx46cqNfVTMgIABfSQTA0mhpLZ3XDACFsJqiqLlSJlYj39gxce+3a5lmNjz/2L2znO3Orklk/lFLO7NYgiSgunWNBSCmQnHQ2Z6XTMxKM0jdVQFuHc9c++vc//cm34zErny/OmtU4d+6cQqE4NDRKCEEIcS4iph6NRsrK4rlcQVWVmGX1dPf1D42Wx6KFkBGAAddPI0hYEUWhhaJNhXCEIIDKNaW/f+i22zbefdet993/pZ985z8WViU9xqfLJDCFHmM87ni24+qqrmOqg2JiKiWSUl6gxVSSgKmQvcPpSx64/+/+9q9XLF/yyfs/KoTo6OiZM6epvDwZi0Wnj+pCSCEE5zwIwpgV1TR1795DTIgcglAIh4seN5BSCi58xqNh6AiZ8llMocj316xZfvvtf3XLpk88+7sXUGXV6aKnwlnlTyGRQrETsMNGGdEjOlZMoulYsahmUIoBkETkqqna6LTIMAbH9boq6v753//Z1DTf9xvqa5csXdje3ul5/sKFc8OQjY2NW1ZEiEkyfD8IggAAIhFzLJXKpLMC41gYdLsBAnBNM/SDrO0uVnGc0k7HazW0No9FG+pe+ONrPT0DdfW1LAiWKTKu0lLeLxEiACYlkrFXukY+8fijixKRsmKRj4xGFdKgKVuG06aCkxo5V4UkQiomI3n7orWXVyRiTz31/aLrRa3ososW3nrrDTU1VdXVlUEQAsDMs51pGpWV5ULI7p6+48fbDVWN1dbEDX2+oUpKVMOIxaKGoXlC1mjUoqTb8fuYbD/ZYZpma0uz7biUsbiCxRR6isEO+bvD6e0Qaf3QLTded1Uuk4sZekJTKhRyvOjlamo6pAJc4HPYjxDiUliGfuLwiZzrHTvWdtNVm55++qftvYOqqhaLNmMcY+CcIwApJcbY8/xMJkcISSRixYJdLDjxuEUpGVH1D1XGzJhlRgxCsGEYmBCT4GuSFiCpx626mqpoxFQo9RkzBLMo4VKWwojk4pVxJ37rrR/98oNPPvFIR0fPH/60tbyqwoyY/elcx+LFn/78/W40lvJCeg56QCjkoioWyZw+fu+1ty9Yf1Umm3vpsa9v+eHPYHbL+vWXh2HY2zsohJRSAgDnfGIiU1lZXrIKM2KWJWIYY4xQ3rJeSg2LREVFWQIYUxiv496GZHRfwXXq6hoM03PdXGpccuF4/jwFqQS7QiKJIgp5vXfib//1ic9/5uPZTF43tMef+N6wkJvbOisC5gZh7ayG7OhYdeuczr0psva8/gBGiAtZa5nl+czAgUNSUS+vSRRHxsj8BVddvnrnzr179x5Klid0TSMEp9NZ3dBj0YiUslAoYowtK8qFQAhpmnZoPKtZlqYqSMoQQaVrXxJR/7V75MEnH732qkuy+fw/fe1hTVN37DrQoOC5UcUX0tKUo4Njtbff/pGPbHr2939qbKrfsuXNxYvmffHhz7Ka6h1jmfZ0PtLcxLhUNW28YJ8x4pm6BICYkKqqNkaNOgUOTBQzC5Zctf7KV1/58+ubtzc1NcRjVslkcrlCWSJeMofUeLqyIokxlCYHCMECQAhhmgaS0hOy0nV6HH8/1pDrqrpaXVXR2zvw/H++KqV0uGhRwVKVTME5YVV88fFHDuzZf8mlK3fv3n/s2ElFUXbs2LXhmrUnegeW3biht7svEok4tpMO+VkEnKNLQkop0dtp57AdVhha167d9HRbTjPLqyqVqTjgup5lRaVEGANVaCqVNk2DUiqlkFLqupbOZE1DBwCF4FNDY6NWTCCpqko+X0yl0idOnLYLDgDkPX+Ritrt8JcDma/+69fsXG7RkgXZTE7TtEjEOH78FKWUc77nvf0KQFdnz9Kli156cYvScep9CZi+nBNR1sT1Br84T4VFFbG+vOtGrKihlWK543rRiIkQwhgDQpxz23EpIbqmciEIIUII23YTZfFcNl/kIhqzCrlCELLW1lnZbO7YsbZEPJZ3/SonzxDssMVHa5PqvJaLLl42OjhcU1O9aNG8xsb65qYGVVXy+cLY2ERzc2NnZ++E7Y4eOboxplwgDlyAHkCqQgVgKVEUxKGsmyxPcs4d1ysWnXjcklJyzjPZPFWoqqqp8TRj3DQNANA0LZPNCS4KBXtWc30YMiHlunWX+X4AAFJK3w8EY+OpibC2oayu+qLAMU+f3pV3l1++etmSBUEQKqoajVnJyor9h45NjKcrqiu3b3sH9uy6LKEThcKj85Z9AAEzynkUQzZgz4wHsxe0qpTatjMxkVFURde1IAhd16OUKIqCEHJdDwMkk2WJuOX5fmdXX3NTPQBKpdJV1ZVhGCaTid7egVLZ3XHcWCLeWFsVCFkYGv6ygRwhT1TXdetmxLKcYjGXyTDXK+TyDHDcMvnA4MaqqEQyFPJ9JXAWephsZ2AAj4t2QeLJMoKxpmmJRDyXyzuOp+uaEBIQKKqCENI0FQHk84VcLq9pmq5p/QNDQRCqqjo6mspmcw88cO/y5Us0Te3q6isvL6upqghDRjFGpnkoXWhRSW9337NHjs0a7m+YGKnMZ5r8Ygv3fE2vKovNd7OaqpQ6fxT9ZeWZjtASYQCD4BTjjGPBBSKkVNcvT5b19A54nq/rmut6BmOEYIwxISQSMcOQjaUmwpBJKeNxa2xsoqmp7vvf/0Z5efKpp35QKBQrK5KWFQ1DhjF2PM93PTsW/7kU+SjSiuGspFWjU59LBIgHIcGYIwjlWW3SDxjIQqWkCoPHxO6M084pVhXBGUJKSYkJIXNmN5byopAxzjjjPAzCMAiFlEKIwA+8IIxZkcHBkfXrL//Wt/6xtra6q6t39+4DruOVTsMAgDHmjKczOUNTK2urM6mJ2yv0cortkE8O9UwNds0sh15YAmcYP1nrxbYfPjfhy5q6qErZRCYMmWFM7lFVBaYK89MfStUKJIQjUWxwQFdxf03l3997x1/fd7fvB2EYdnR0e55fV1cThiEAAMDERIYxVlVZzgFOn2jfaMr5lulygWeOepy36Pm3Zs6gYAQqQafz3k6b5zSzlkIxXyjxtaQ/UiIpxQUolwgAYQSUYsUwVnLvwW8+ev26y0dGU4TQdDrzve/9OGZZjJXm5UpylmEQctMY6h24MYLmx0ybCTxVtJIz8c3IIy80MzdVZFcwCCFeHXNeC9WgrJxIWXQ8TAjBWEoZhqEQUgguJpec+VeScsiZCFkRsHC9oa7esfGMbbuqqnzvez8ZGhzVNLXULyu1zMoSMStu9Xb1Xm/IBXFjGv0URybFIIQgcGZu4gISKB03CUAmYK+m/Vw0HqFY8slXBVwUbUfTVIwxpXSaf2fLASbSOUPXXccNpazQlFAIJ5PlQiApX35565Ytb5aVxYMgOPMFAM8NewdHrtTRkoRRCM9Gj5CCQQISUuq6lme8GmNbcDi/yTd9KNMw6nL5uGklCHAhCSEKJdlsXlVVKeX4RMayIo7rUUpVhVJKCcEIlWwaO45j247gIgxDICTkvChRwPjhY20Hdx/49a+fJ5QILrjgU5YDUvChiUzcd1fWRc/lPUKAUD7km0ed+ctnL5rT+POTXWHWmRvTAyHh0XnL4EIEqBieG8rnyqsokuFUd63kc4QQYRhyLoQQAEhKCYAVhWKMCcaYECml47iGoTu2o6gqRTJRLIQV5YamHu0eBCla5zQbulZqWJSO8JmiMzEydlel1miopZrhTEgGwQNeeMoOy3QVScGkrCBQq1MmL1RWkQgZBB/NeScDVM8ZB8ynB14AGGMIIUqpooCUMghCzjkhuNQDdl1PSkkpKfV9pZSh60rPmx1VW4N8JSJJi27P+ul8MRqGpYo556LgB36+sKlMbTY1hwuMEAYkJBISUYzyTBzO+xfH9TqdMiEBYQDEJQRSXliFAJDP+UGX19fVuIWCqqmTPnHmNoBSKZNSomnqpD1RAABCsOv6iqJQSsIgtEO+JqreXB11hBQIrje1hEq35/OpIhEAEiERhg0Ura/UGwzFYcIgAAg5XBBAOsGAUMrnL40Vm0ylTMGlalxJcmd3aKaWQEgD6HGCrKKbnNmOiwAB4Jlzv1LKMGQAiFI6ebycdmoYu64nhMQYM8ZLovOl9IX0uSAARYGWx/RWUxnzmStkKGRc0ZoMFQAFAhkEOu3gcCEYF0gFaNXgiqRBARHTPF4MrkkaTEiAsxwpPT8ME0DdHhdEdx2HUuK6vq5rU/qFOGecC0oJIVhKyZiAGYHRdW2EkKaVRlIkF8LUtGNFf4Ub1hpKICQgFEhpUDxH0abtzROCSBBSvDDqnPQR0VROQCDZ7/G+EbuRophhngr8FQGLKJTLs8YOLlDYYkKOMSmE8P0QYwKAfD8QQnLOgyBgjJeqWoxxzkXpfikQFIs2AOi6JiUqESAlEowxRXk74+IZr+AS+Vy6XLpcelwoAIEQvxkuHmdE0zUZBC3SX4CCuBQDSHk7F0IYFDFttwP1vFbTuSoEgHwu8xwJYFxwBIAx9v0AAJXCEyFEiHNa/iAlD0NWumCMz2g7yyBkpkJPOfJw3lse0x0+OaI0rQkEQSDk74eLw4gqgDTXublcVzEe8MKs5+eDUDd0xgUC0uWLVVMh8n0JwAgcKTwhBWdSSImFlEAI8f1AURSEztL4EkohWBAwTZt8Oj2QhRCSEpW8k6LQbWm3yVCipUnuGW4PA7w2UuxlyFIB+d5H6qwjheCtjI0VqhFFwWhy6JuzMS7zTESmqi+l9V90G44XOoXq/wAAACV0RVh0ZGF0ZTpjcmVhdGUAMjAyMS0wOC0xN1QxNjowMTo1MyswMDowMMNqbe0AAAAldEVYdGRhdGU6bW9kaWZ5ADIwMjEtMDgtMTdUMTY6MDE6NTMrMDA6MDCyN9VRAAAAAElFTkSuQmCC

// @require      https://openuserjs.org/src/libs/sizzle/GM_config.js
// @require      https://cdn.jsdelivr.net/npm/axios/dist/axios.min.js
// @require      https://code.jquery.com/jquery-3.6.0.min.js

// @grant        GM_getValue
// @grant        GM_setValue
// @grant        GM_registerMenuCommand
// @grant        GM_addStyle

// @updateURL    https://bitbucket.org/REIONE/boorutags/raw/main/BooruTags.user.js
// @downloadURL  https://bitbucket.org/REIONE/boorutags/raw/main/BooruTags.user.js

// @match        *://chan.sankakucomplex.com/post/show/*
// @match        *://chan.sankakucomplex.com/*/post/show/*
// @match        *://chan.sankakucomplex.com/posts/show/*
// @match        *://chan.sankakucomplex.com/posts/*
// @match        *://chan.sankakucomplex.com/*/posts/show/*
// @match        *://chan.sankakucomplex.com/*/posts/*

// @match        *://*.danbooru.donmai.us/posts/*
// @match        *://gelbooru.com/index.php?page=post&s=view&id=*
// @match        *://yande.re/post/show/*
// @match        *://konachan.com/post/show/*
// @match        *://hentaichan.live/manga/*
// @match        *://hchan.live/manga/*
// @match        *://saucenao.com/*
// @match        *://iqdb.org/*
// @match        *://e621.net/posts/*
// @match        *://rule34.xxx/*
// @match        *://e-hentai.org/*

// ==/UserScript==

(function () {
    "use strict";

    const booruTagsConfig = {
        groupTag: {
            label: "Group tag",
            section: ["VK"],
            type: "text",
            default: "@rhentai",
        },
        groupId: {
            label: "Group ID",
            type: "int",
            default: "-165681907",
            // default: -157938173,
        },
        botToken: {
            label: "Access token (<a href='https://vkhost.github.io' target='_blank'>https://vkhost.github.io</a>)",
            type: "text",
            default:
                "vk1.a.4r_ycqYVRFolTI-bCGjkHDCUYUjRNGCpK53QYNCsNanbplhYFwDfVkuRrA0UrRZJMzai2ObIYti8g66Uvc6omRSIBTVFiD8iF3BPvdoh78AyuxfIOYzd7rbYUBQDnqbp-av1rmBl__q_HEsp2-s-npV8K1UcJex-pArUluUya6qwoZ2CvUSnAwAhVq46Qjvh",
        },
        channelList: {
            label: "Channel list",
            placeholder: "Channel ID/Bot Token",
            section: ["Telegram"],
            type: "channelList",
            default: [
                {
                    name: "RealHorny",
                    username: "@realhorny",
                    id: "-1001536498966",
                    bot_token: "6110311126:AAGHVgZWSNV6bd5v7QPFnjs7MXSG1oO3xP4",
                },
                {
                    name: "RHENTAI",
                    username: "@realhentai",
                    id: "-1001128185238",
                    bot_token: "5101680877:AAEkDyjxAGqspvGGvCq6nGpGLS9ZR_nIpWA",
                },
                {
                    name: "HornyFuta",
                    username: "@hornyfuta",
                    id: "-1001615089853",
                    bot_token: "6036834746:AAFLJNVTNwSCLrHsb1kq7T5mnIsAIUwbBYg",
                },
            ],
        },
    };

    //myConfig.dropdown.default = myConfig.dropdown.options[0];

    function initSettings() {
        GM_config.init({
            id: "booruTagsConfig",
            title: "",
            fields: booruTagsConfig,
            events: {
                init: function () {
                    console.log(GM_config.get("channelList"));
                    //GM_config.get("channelList").forEach(item => console.log(item))
                },
                save: function () {
                    GM_config.close();
                },
                close: function () {},
            },
            types: {
                // dropdown: {
                //   toNode: function () {
                //     let container = document.createElement("div"),
                //       label = document.createElement("label"),
                //       select = document.createElement("select"),
                //       self = this;
                //     container.classList.add("config_var");
                //     label.classList.add("field_label");
                //     label.textContent = self.settings.label;

                //     self.settings.options.forEach(option => {
                //       let optionElement = document.createElement("option");
                //       optionElement.value = option;
                //       optionElement.textContent = option;
                //       select.appendChild(optionElement);
                //     });

                //     select.value = this.value;

                //     // select.addEventListener("change", function () {
                //     //   GM_config.fields.dropdown.value = select.value;
                //     //   GM_config.save();
                //     // });

                //     container.appendChild(label);
                //     container.appendChild(select);

                //     return container;
                //   },
                //   toValue: function () {
                //     return ["1", "2"];
                //     //return node.querySelector("select").value;
                //   },
                //   reset: function () {
                //     //node.querySelector("select").value = this.default;
                //   }
                // },
                channelList: {
                    default: {},
                    toNode: function () {
                        let container = document.createElement("div"),
                            label = document.createElement("label"),
                            inputContainer = document.createElement("div"),
                            input = document.createElement("input"),
                            addButton = document.createElement("button"),
                            list = document.createElement("ul"),
                            self = this;

                        console.log(this.value.slice());
                        self.newValue = JSON.parse(this.value.slice()) ?? this.value.slice();

                        container.classList.add("config_var");
                        label.classList.add("field_label");
                        inputContainer.classList.add("input_container");
                        label.textContent = this.settings.label;
                        input.type = "text";
                        input.placeholder = self.settings.placeholder;
                        addButton.textContent = "Add";

                        addButton.addEventListener("click", async function () {
                            let item = input.value.trim().split("/");
                            if (item) {
                                const bot = new TelegramBotApi(GM_config.get(item[1]));
                                const getChat = await bot.getChat({ chat_id: item[0] }),
                                    channelName = getChat.result.title,
                                    channelUsername = getChat.result.username;
                                console.log(getChat);
                                self.newValue.push({ name: channelName, username: channelUsername, id: item[0], token: item[1] });
                                self.update();
                                input.value = "";
                            }
                        });

                        self.update = function () {
                            if (self.value.length == 0) self.value = self.newValue = self.default.slice();
                            GM_setValue("channelsList", self.value);
                            list.innerHTML = "";
                            self.newValue.forEach((item, index) => {
                                let li = document.createElement("li"),
                                    text = document.createElement("div"),
                                    removeButton = document.createElement("button");
                                text.textContent = `${item["name"]} ${item["username"]}`;
                                removeButton.textContent = "Remove";
                                removeButton.classList.add("remove-btn");
                                removeButton.addEventListener("click", function () {
                                    li.remove();
                                    self.newValue = self.newValue.filter((e) => e["id"] !== item["id"] && e["token"] == item["token"]);
                                });
                                li.draggable = true;
                                li.addEventListener("dragstart", function (event) {
                                    event.dataTransfer.setData("text/plain", index.toString());
                                });
                                li.addEventListener("dragover", function (event) {
                                    event.preventDefault();
                                });
                                li.addEventListener("drop", function (event) {
                                    event.preventDefault();
                                    const sourceIndex = parseInt(event.dataTransfer.getData("text/plain"));
                                    const targetIndex = index;
                                    if (sourceIndex !== targetIndex) {
                                        const [removed] = self.newValue.splice(sourceIndex, 1);
                                        self.newValue.splice(targetIndex, 0, removed);
                                        self.update();
                                    }
                                });

                                li.appendChild(text);
                                li.appendChild(removeButton);
                                list.appendChild(li);
                            });
                        };

                        container.appendChild(label);
                        inputContainer.appendChild(input);
                        inputContainer.appendChild(addButton);
                        container.appendChild(inputContainer);
                        container.appendChild(list);

                        self.update();
                        GM_setValue("channelsList", self.newValue);

                        return container;
                    },
                    toValue: function () {
                        // let values = [],
                        //   list = this.wrapper.querySelectorAll("li div");
                        // list.forEach((item) => values.push(item.textContent));
                        console.log("Test: " + this.newValue);
                        return this.newValue;
                    },
                    reset: function () {
                        let input = this.wrapper.querySelector("input"),
                            list = this.wrapper.querySelectorAll("li");
                        list.innerHTML = "";
                        this.newValue = this.default.slice();
                        console.log(GM_config.get("channelList"));
                        this.update();
                        input.value = "";
                    },
                    read: function () {
                        GM_setValue("channelsList", self.newValue);
                        return self.newValue;
                    },
                },
            },
            css: `
      * {
        box-sizing: border-box;
      }
      a {
        color: #fff;
      }
      a:hover {
        color: #f0f0f0;
      }
      ::-webkit-scrollbar {
        width: 0.5em;
      }
      ::-webkit-scrollbar-thumb {
        background-color: #404040;
        border-radius: 1em;
      }
      ::-webkit-scrollbar-thumb:hover {
        background-color: #aaa;
        box-shadow: 0 0 2px 1px rgba(0, 0, 0, 0.2);
      }
      ::-webkit-scrollbar-track {
        background-color: #343434;
        border-radius: 0;
      }
      ::-webkit-scrollbar-track:hover {
        box-shadow: inset 0 0 2px 1px rgba(0, 0, 0, 0.1);
      }
      .config_var {
        margin: 0;
        display: flex;
        flex-direction: column;
        gap: 0.5rem;
      }
      body {
        padding: 1rem;
        background: #343434 !important;
        color: #fff !important;
        font-size: 24px !important;
      }
      .config_header {
        display: none !important;
      }
      .section_header {
        border: none !important;
        margin: 0 0 1rem 0 !important;
        border-radius: 0.5rem;
        padding: 0.25rem;
    }
    .section_header_holder {
      display: flex;
      flex-direction: column;
      gap: 0.25rem;
    }
      .saveclose_buttons,
      .reset,
      button {
        display: inline-block;
        font-family: sans-serif !important;
        font-size: 12px;
        font-weight: 300;
        color: #0a0 !important;
        opacity: 1;
        cursor: pointer;
        padding: 0.5rem 1rem !important;
        text-align: center;
        background-color: #e5f6e5;
        -webkit-border-radius: 1rem;
        border-radius: 1rem;
        margin: 0.5rem !important;
        border: none;
        text-decoration: none !important;
      }
      .saveclose_buttons:hover,
      .reset:hover,
      button:hover {
        background-color: #0a0 !important;
        color: #fff !important;
    }
      div[id*='_buttons_holder'] {
        background: #343434;
        left: 0;
        position: fixed;
        right: 0;
        bottom: 0;
        display: flex;
        flex-direction: row-reverse;
      }
      div[id*='_wrapper'] {
        padding-bottom: 3rem;
        display: flex;
        flex-direction: column;
        gap: 0.5rem;
      }
      .input_container {
        display: flex;
        background: white;
        border-radius: 1rem;
        width: 100%;
        height: fit-content;
      }
      .input_container button {
        margin: 0.25rem !important;
      }
      input,
      select {
        width: 100%;
        border-radius: 1rem;
        padding: 0.75rem;
        border: 0;
        outline: 0;
      }
      ul {
        list-style: none;
        padding: 0;
        margin: 0;
        display: flex;
        gap: 0.5rem;
        flex-direction: column;
      }
      .remove-btn {
        float: right;
        margin: 0 !important;
      }
      li {
        padding: 0.5rem;
        font-size: 14px;
        display: flex;
        justify-content: space-between;
        align-items: center;
      }`,
        });

        GM_registerMenuCommand("Settings", openSettings);

        function openSettings() {
            GM_config.open();
        }
    }

    initSettings();
    init();
    initSaucenaoButtons();
    initBooruButtons();

    function init() {
        switch (location.host) {
            case "hchan.live":
            case "hentaichan.live":
                initHenchanButtons();
                break;
            case "saucenao.com":
                initSaucenaoButtons();
                break;
            case "iqdb.org":
                initIqdbButtons();
                break;
            case "e-hentai.org":
                initEhentaiButtons();
                break;
        }
    }

    function getTags(links, tag = true, mode = 1) {
        if (mode === 1) links.set("general", []);

        let tags = {
            artists: [],
            characters: [],
            copyrights: [],
            general: [],
            meta: [],
        };

        links.forEach((value, key) => {
            value.forEach((link) => {
                link = link.textContent || link;

                let text = link.toLowerCase().replace(/[^0-9A-Za-zА-Яа-яёЁ\s\(\)\:\-\d\/\~\&\.\_]/g, "");
                if (key != "artists") text = text.replace(/\(.*\)/, "");
                text = text
                    .trim()
                    .replace(/[\&]/g, "_and_")
                    .replace(/[\s\:\-\/\~\.]/g, "_")
                    .replace(/__/g, "_")
                    .replace(/^\_+|\_+$/g, "");
                if (key != "artists") {
                    text = "#" + text + GM_config.get("groupTag");
                } else {
                    text = text.replace(/\)/g, GM_config.get("groupTag") + ")").replace(/\_\(/g, GM_config.get("groupTag") + " (#");
                    text = "#" + text;
                    if (text.indexOf("(") == -1) text = text + GM_config.get("groupTag");
                    text = text
                        .replace(new RegExp("\\)" + GM_config.get("groupTag"), "g"), ")")
                        .replace(/\)\s\(/g, " ")
                        .replace(/\_\@/g, "@");
                }
                text = text.replace(/\_+(?=\_)/g, "");
                // if (!tag) text = text.replace(new RegExp(GM_config.get('groupTag')), '')
                if (!tag) {
                    const regex = new RegExp(GM_config.get("groupTag"), "g");
                    text = text.replace(regex, "");
                }

                if (key == "artists") tags.artists.push(text);
                if (key == "characters") tags.characters.push(text);
                if (key == "copyrights") tags.copyrights.push(text);
                if (key == "general") tags.general.push(text);
                if (key == "meta") tags.meta.push(text);

                Object.keys(tags).forEach(function (key) {
                    tags[key] = uniq(tags[key]);
                });
            });
        });

        if (mode === 1 && tags.copyrights.length == 0 && tags.characters.length == 0 && tags.artists.length == 0) tags.copyrights.push("#unknown" + GM_config.get("groupTag"));

        return tags;
    }

    function copyTags(links, tag = true, mode = 1) {
        let tags = getTags(links, tag, mode);

        let copyText = "";

        if (tags.meta.find((el) => el.includes("animated"))) copyText += tags.meta.find((el) => el.includes("animated")) + " ";
        if (tags.meta.find((el) => el.includes("ai_generated")) || tags.meta.find((el) => el.includes("ai_created")) || tags.meta.find((el) => el.includes("ai-created")))
            copyText += "#ai_generated" + GM_config.get("groupTag") + " ";
        if (tags.meta.find((el) => el.includes("realistic"))) copyText += tags.meta.find((el) => el.includes("realistic")) + " ";
        if (tags.copyrights.length) copyText += tags.copyrights.join(" ") + "\n";
        if (tags.characters.length) copyText += tags.characters.join(" ") + "\n";
        if (tags.artists.length) copyText += "\nby " + tags.artists.join(" ");
        copyText = copyText.replace("\n\n", "\n");
        if (tags.general.length) copyText += "\n\n" + tags.general.join(", ");
        if (!tag) {
            const regex = new RegExp(GM_config.get("groupTag"), "g");
            copyText = copyText.replace(regex, "");
        }
        copyToClipboard(copyText);
        notify(copyText);
    }

    function iqdbButtons(selector) {
        let resultNodes = document.querySelectorAll(selector);
        resultNodes.forEach(async (resultNode) => {
            const jsonObj = await xmlHttpRequest({
                url: resultNode.href + ".json",
                responseType: "json",
            });
            if (jsonObj && (jsonObj.hasOwnProperty("tag_string_character") || jsonObj.hasOwnProperty("tag_string_copyright") || jsonObj.hasOwnProperty("tag_string_artist"))) {
                let copyTagsButton = document.createElement("div");
                copyTagsButton.className = "bt-button bt-button-iqdb";
                copyTagsButton.innerHTML = "Tags";
                copyTagsButton.onclick = function () {
                    let links = new Map();
                    if (jsonObj.hasOwnProperty("tag_string_copyright")) links.set("copyrights", jsonObj.tag_string_copyright.split(" "));
                    if (jsonObj.hasOwnProperty("tag_string_character")) links.set("characters", jsonObj.tag_string_character.split(" "));
                    if (jsonObj.hasOwnProperty("tag_string_artist")) links.set("artists", jsonObj.tag_string_artist.split(" "));
                    copyTags(links, true, 1, 1);
                };
                resultNode.before(copyTagsButton);
            }

            if (jsonObj && jsonObj.hasOwnProperty("source")) {
                let copySourceButton = document.createElement("div");
                copySourceButton.className = "bt-button bt-button-iqdb";
                copySourceButton.innerHTML = "Source";
                copySourceButton.onclick = function () {
                    copyToClipboard(jsonObj.source);
                    notify(jsonObj.source);
                };
                resultNode.before(copySourceButton);
            }
        });
    }

    function initIqdbButtons() {
        iqdbButtons('td.image a[href*="danbooru"]:not(#more1 a)');
        document.querySelector('#show1 a[onclick][href="#"]').onmouseup = function () {
            iqdbButtons('#more1 td.image a[href*="danbooru"]');
        };
    }

    function initSaucenaoButtons() {
        let resultNodes = document.querySelectorAll(".result:not(#result-hidden-notification) .resultmiscinfo");

        resultNodes.forEach((resultNode) => {
            resultNode.querySelectorAll('.resultmiscinfo a[href*="post"]').forEach((link) => {
                link.className = "bt-button";
            });
            resultNode.querySelectorAll("br").forEach((link) => {
                link.remove();
            });

            let copyTagsButton = document.createElement("div");
            copyTagsButton.style = "margin: 5px 0 !important; font-size: 12px;";
            copyTagsButton.className = "bt-button";
            copyTagsButton.innerHTML = "Tags";
            copyTagsButton.onclick = function () {
                let links = new Map();
                resultNode.parentElement.parentElement.querySelectorAll("strong").forEach((s) => {
                    if (~s.innerText.indexOf("Material") || ~s.innerText.indexOf("Copyrights")) links.set("copyrights", nextTextNodes(s));
                    if (~s.innerText.indexOf("Character")) links.set("characters", nextTextNodes(s));
                    if (~s.innerText.indexOf("Creator") || ~s.innerText.indexOf("Artist") || ~s.innerText.indexOf("Author") || ~s.innerText.indexOf("Member")) links.set("artists", nextTextNodes(s));
                });
                copyTags(links);
            };
            resultNode.appendChild(copyTagsButton);

            let sourceNode = null;

            resultNode.parentElement.parentElement.querySelectorAll("strong").forEach((s) => {
                if (~s.innerText.indexOf("Source")) sourceNode = s.nextElementSibling.href;
            });
            if (sourceNode) {
                let copySourceButton = document.createElement("div");
                copySourceButton.style = "margin: 5px 0 !important; font-size: 12px;";
                copySourceButton.className = "bt-button";
                copySourceButton.innerHTML = "Source";
                copySourceButton.onclick = function () {
                    copyToClipboard(sourceNode);
                    notify(sourceNode);
                };
                resultNode.appendChild(copySourceButton);
            }
        });
    }

    function initHenchanButtons() {
        let tagsNodes = document.querySelectorAll("#side li.sidetag a:last-of-type");
        if (tagsNodes.length) return;

        let copyTagsButton = document.createElement("div");
        copyTagsButton.className = "bt-button";
        copyTagsButton.innerHTML = "Copy tags";
        copyTagsButton.onclick = function () {
            let links = new Map();
            links.set("general", tagsNodes);
            let tags = getTags(links, false, 0);

            let tagsText = tags.general.length ? tags.general.join(", ") : "";
            tagsText = tagsText.charAt(1).toUpperCase() + tagsText.slice(2);
            tagsText = tagsText.replace(/[\#]/g, "").replace(/\_/g, " ");

            let mangaInfo = document.querySelector("#info_wrap");
            let copyText = "";
            copyText += `Название: ${mangaInfo.querySelector(".name_row a").text.replace(" (", " / ").replace(")", "")}\n`;
            mangaInfo.querySelectorAll(".row").forEach((row) => {
                if (row.children[0].innerText == "Аниме/манга") copyText += `Серия: ${row.children[1].innerText}\n`;
                if (row.children[0].innerText == "Автор") copyText += `Автор: ${row.children[1].innerText}\n`;
                if (row.children[0].innerText == "Переводчик") copyText += `Перевод: ${row.children[1].innerText}\n`;
            });
            copyText += `Теги: ${tagsText}\n`;
            copyToClipboard(copyText);
            notify(copyText);
        };
        document.querySelector("#side .sidetags").before(copyTagsButton);
    }

    function initEhentaiButtons() {
        const taglist = document.querySelector("#taglist");
        if (!taglist) return;

        let copyTagsButton = createButton("Copy Tags", () => {
            const links = new Map();
            links.set("copyrights", document.querySelectorAll(`a[id*="ta_parody:"]`));
            links.set("characters", document.querySelectorAll(`a[id*="ta_character:"]`));
            links.set("artists", document.querySelectorAll(`a[id*="ta_cosplayer:"]`));
            links.set("general", document.querySelectorAll(`a[id*="ta_female:"]`));
            const tags = getTags(links, false, 0);

            let copyText = "#cosplay\n";

            // if (tags.meta.find((el) => el.includes("video"))) copyText += tags.meta.find((el) => el.includes("video")) + " ";
            // if (tags.meta.find((el) => el.includes("gif"))) copyText += tags.meta.find((el) => el.includes("gif")) + " ";
            // if (tags.meta.find((el) => el.includes("animated"))) copyText += tags.meta.find((el) => el.includes("animated")) + " ";
            // if (tags.meta.find((el) => el.includes("ai_generated")) || tags.meta.find((el) => el.includes("ai_created")) || tags.meta.find((el) => el.includes("ai-created"))) copyText += "#ai_generated ";
            // if (tags.meta.find((el) => el.includes("realistic"))) copyText += tags.meta.find((el) => el.includes("realistic")) + " ";
            if (tags.copyrights.length) copyText += "__Parody:__ " + tags.copyrights.join(" ") + "\n";
            if (tags.characters.length) copyText += "__Character:__ " + tags.characters.join(" ") + "\n";
            if (tags.artists.length) copyText += "__Cosplayer:__ " + tags.artists.join(" ");
            if (tags.general.length) copyText += "\n__Tags:__ " + tags.general.join(" ");

            // if (sourceLink) copyText += `\n\nSource ${sourceLink}`;

            copyToClipboard(copyText);
            notify(copyText);
        });
        taglist.appendChild(copyTagsButton);
    }

    function initBooruButtons() {
        let sidebarNode = document.querySelector('.sidebar div[style="margin-bottom: 1em;"], section#tag-list, .sidetags, ul#tag-list.tag-list, #tag-sidebar');

        if (!sidebarNode) return;

        let links = new Map();
        links.set(
            "artists",
            document.querySelectorAll(`.tag-type-artist a[href*="?tags="][itemprop][id], .artist-tag-list .search-tag[href*="/posts?tags="],
                  li.tag-type-artist a[href*="index.php?page=post&s=list&tags="], .tag-type-copyright .tag`)
        );
        links.set(
            "characters",
            document.querySelectorAll(`.tag-type-character a[href*="?tags="][itemprop][id], .character-tag-list .search-tag[href*="/posts?tags="],
                  li.tag-type-character a[href*="index.php?page=post&s=list&tags="], .tag-type-character .tag`)
        );
        links.set(
            "copyrights",
            document.querySelectorAll(`.tag-type-copyright a[href*="?tags="][itemprop][id], .copyright-tag-list .search-tag[href*="/posts?tags="],
                  li.tag-type-copyright a[href*="index.php?page=post&s=list&tags="], .tag-type-artist .tag`)
        );
        links.set(
            "general",
            document.querySelectorAll(`.tag-type-general a[href*="?tags="][itemprop][id], .general-tag-list .search-tag[href*="/posts?tags="],
                  li.tag-type-general a[href*="index.php?page=post&s=list&tags="], .tag-type-artist .tag`)
        );
        links.set(
            "meta",
            document.querySelectorAll(`.tag-type-medium a[href*="?tags="][itemprop][id], .meta-tag-list .search-tag[href*="/posts?tags="],
                  li.tag-type-meta a[href*="index.php?page=post&s=list&tags="]`)
        );

        let copyTagsButton = document.createElement("div");
        copyTagsButton.className = "bt-button";
        copyTagsButton.innerHTML = "Copy tags";
        copyTagsButton.onclick = function () {
            copyTags(links);
        };
        sidebarNode.before(copyTagsButton);

        let sourceNode = document.querySelector("a[target=_blank][rel][href], li#post-info-source a[rel], #stats li a[href][rel]");
        let sourceLink;

        if (sourceNode) {
            sourceLink = sourceNode.href;
            let copySourceButton = document.createElement("div");
            copySourceButton.className = "bt-button";
            copySourceButton.innerHTML = "Copy source";
            copySourceButton.onclick = function () {
                copyToClipboard(sourceNode.href);
                notify(sourceNode.href);
            };
            sidebarNode.before(copySourceButton);
        }

        let copyTagsForTGButton = document.createElement("div");

        copyTagsForTGButton.className = "bt-button";
        copyTagsForTGButton.innerHTML = "Copy tags for Telegram";
        copyTagsForTGButton.onclick = async function () {
            let tags = getTags(links, false, 1);

            let copyText = "";

            if (tags.meta.find((el) => el.includes("video"))) copyText += tags.meta.find((el) => el.includes("video")) + " ";
            if (tags.meta.find((el) => el.includes("gif"))) copyText += tags.meta.find((el) => el.includes("gif")) + " ";
            if (tags.meta.find((el) => el.includes("animated"))) copyText += tags.meta.find((el) => el.includes("animated")) + " ";
            if (tags.meta.find((el) => el.includes("ai_generated")) || tags.meta.find((el) => el.includes("ai_created")) || tags.meta.find((el) => el.includes("ai-created")))
                copyText += "#ai_generated ";
            if (tags.meta.find((el) => el.includes("realistic"))) copyText += tags.meta.find((el) => el.includes("realistic")) + " ";
            if (tags.copyrights.length) copyText += tags.copyrights.join(" ") + "\n";
            if (tags.characters.length) copyText += tags.characters.join(" ") + "\n";
            if (tags.artists.length) copyText += "by " + tags.artists.join(" ");

            if (tags.general.length) copyText += "\n\n" + tags.general.join(", ");

            if (sourceLink) copyText += `\n\nSource ${sourceLink}`;

            copyToClipboard(copyText);
            notify(copyText);
        };

        sidebarNode.before(copyTagsForTGButton);

        let downloadNode = document.querySelector(
            'li#post-option-download a[download], #stats a#highres[itemprop="contentUrl"], ul#tag-list.tag-list a[target="_blank"][rel="noopener"][style="font-weight: bold;"], a.original-file-unchanged#png, .original-file-changed#highres, a[href*="//images/"][style="font-weight: bold;"]'
        );

        if (downloadNode) {
            let downloadButton = document.createElement("div");
            downloadButton.className = "bt-button";
            downloadButton.innerHTML = "Download";
            downloadButton.onclick = async function () {
                let downloadFileName = decodeURI(downloadNode.href.split("/").pop().split("?")[0]);
                let timeStart = new Date().getTime();
                notify("Загружается", ["warning", "not_hide"], "download_started");
                let blob = new Blob([await xmlHttpRequest({ url: downloadNode.href, responseType: "blob" })]);
                console.log((blob.size / Math.pow(1024, 2)).toFixed(2));
                let timeEnd = new Date().getTime();
                let time = timeEnd - timeStart;
                removeNotification(document.querySelector("#download_started"), 300);
                notify(`Успешно загружено за ${time / 1000} секунд`);
                let downloadLink = document.createElement("a");
                let blobUrl = URL.createObjectURL(blob);
                downloadLink.href = blobUrl;
                downloadLink.download = downloadFileName;
                document.body.appendChild(downloadLink);
                downloadLink.click();
            };
            sidebarNode.before(downloadButton);
        }

        let sendToTGButton = document.createElement("div");
        sendToTGButton.className = "bt-button";
        sendToTGButton.innerHTML = "Send to VK/Telegram";
        sendToTGButton.onclick = async function () {
            document.getElementById("myModal")?.remove();

            const modal = document.createElement("div");
            modal.id = "myModal";
            modal.className = "bt-modal";

            const modalContent = document.createElement("div");
            modalContent.className = "bt-modal-content";

            const header = document.createElement("h2");
            header.textContent = "Confirm post";

            const socialSelectContainer = document.createElement("div");
            socialSelectContainer.className = "bt-select";

            const selectedSocial = await GM_getValue("selectedSocial");

            // const socialSelectBtn = document.createElement("button");
            // socialSelectBtn.className = "bt-select";

            ["VK", "Telegram"].forEach((option) => {
                const btn = document.createElement("button");
                btn.type = "button";
                btn.innerText = option;
                btn.className = "bt-button";
                btn.classList.add("bt-button--variant-select");
                if (option === selectedSocial) btn.classList.add("bt-button--variant-select-actived");
                btn.onclick = function () {
                    socialSelectContainer.childNodes.forEach((child) => child.classList.remove("bt-button--variant-select-actived"));
                    btn.classList.add("bt-button--variant-select-actived");
                    GM_setValue("selectedSocial", btn.innerText);
                    checkSocial();
                };
                socialSelectContainer.appendChild(btn);
                // return `<button type="button" class="bt-button" ${isSelected}>${option['username']}</button>`;
            });

            // const socialSelectButtons = ["VK", "Telegram"].map(option => {
            //   const isSelected = option === GM_getValue("selectedSocial") ? 'bt-button--variant-select-actived' : '';
            //   return `<button type="button" class="bt-button bt-button--variant-select ${isSelected}">${option}</button>`;
            // }).join('');
            // socialSelectContainer.innerHTML = socialSelectButtons;

            const channelSelectContainer = document.createElement("div");
            channelSelectContainer.className = "bt-select";
            channelSelectContainer.style.display = "none";

            const selectedChannel = await GM_getValue("selectedChannel");

            // const channelSelectBtn = document.createElement("select");
            // channelSelect.className = "bt-select";
            GM_getValue("channelsList").forEach((option) => {
                const btn = document.createElement("button");
                btn.type = "button";
                btn.innerText = option["username"];
                btn.className = "bt-button";
                btn.classList.add("bt-button--variant-select");
                if (option["username"] === selectedChannel) btn.classList.add("bt-button--variant-select-actived");
                btn.onclick = function () {
                    channelSelectContainer.childNodes.forEach((child) => child.classList.remove("bt-button--variant-select-actived"));
                    btn.classList.add("bt-button--variant-select-actived");
                    GM_setValue("selectedChannel", btn.innerText);
                };
                channelSelectContainer.appendChild(btn);
                // return `<button type="button" class="bt-button" ${isSelected}>${option['username']}</button>`;
            });
            //socialSelectContainer.innerHTML += channelSelectButtons;

            // const socialSelect = document.createElement("select");
            // socialSelect.className = "bt-select";
            // const socialSelectOptions = ["VK", "Telegram"].map(option => {
            //   const isSelected = option === GM_getValue("selectedSocial") ? 'selected' : '';
            //   return `<option value="${option}" ${isSelected}>${option}</option>`;
            // }).join('');
            // socialSelect.innerHTML = socialSelectOptions;
            // socialSelect.addEventListener("change", function () {
            //   const selectedOption = socialSelect.value;
            //   console.log(selectedOption);
            //   checkSocial();
            //   GM_setValue("selectedSocial", selectedOption);
            // });

            function checkSocial() {
                switch (GM_getValue("selectedSocial")) {
                    case "VK":
                        channelSelectContainer.style.display = "none";
                        break;
                    case "Telegram":
                        channelSelectContainer.style.display = "block";
                        break;
                }
            }

            // const channelSelect = document.createElement("select");
            // channelSelect.className = "bt-select";
            // const channelSelectOptions = GM_config.get("channelList").map(option => {
            //   const isSelected = option['username'] === GM_getValue("selectedChannel") ? 'selected' : '';
            //   return `<option value="${option['username']}" ${isSelected}>${option['username']}</option>`;
            // }).join('');
            // channelSelect.innerHTML = channelSelectOptions;
            // channelSelect.addEventListener("change", function () {
            //   const selectedOption = channelSelect.value;
            //   console.log(selectedOption);
            //   GM_setValue("selectedChannel", selectedOption);
            // });

            const btnContainer = document.createElement("div");
            btnContainer.className = "bt-container";

            const confirmBtn = document.createElement("button");
            confirmBtn.id = "confirmBtn";
            confirmBtn.className = "bt-button";
            confirmBtn.textContent = "Confirm";

            const cancelBtn = document.createElement("button");
            cancelBtn.id = "cancelBtn";
            cancelBtn.className = "bt-button";
            cancelBtn.textContent = "Cancel";

            modalContent.appendChild(header);
            modalContent.appendChild(socialSelectContainer);
            modalContent.appendChild(channelSelectContainer);
            // modalContent.appendChild(socialSelect);
            // modalContent.appendChild(channelSelect);
            btnContainer.appendChild(cancelBtn);
            btnContainer.appendChild(confirmBtn);
            modalContent.appendChild(btnContainer);

            modal.appendChild(modalContent);
            document.body.appendChild(modal);

            checkSocial();

            confirmBtn.addEventListener("click", async () => {
                document.getElementById("myModal")?.remove();

                let tags,
                    copyText = "";

                switch (GM_getValue("selectedSocial")) {
                    case "VK":
                        tags = getTags(links, true, 1);

                        copyText = "";

                        if (tags.meta.find((el) => el.includes("animated"))) copyText += tags.meta.find((el) => el.includes("animated")) + " ";
                        if (tags.meta.find((el) => el.includes("ai_generated")) || tags.meta.find((el) => el.includes("ai_created")) || tags.meta.find((el) => el.includes("ai-created")))
                            copyText += "#ai_generated" + GM_config.get("groupTag") + " ";
                        if (tags.meta.find((el) => el.includes("realistic"))) copyText += tags.meta.find((el) => el.includes("realistic")) + " ";
                        if (tags.copyrights.length) copyText += tags.copyrights.join(" ") + "\n";
                        if (tags.characters.length) copyText += tags.characters.join(" ") + "\n";
                        if (tags.artists.length) copyText += "\nby " + tags.artists.join(" ");
                        copyText = copyText.replace("\n\n", "\n");
                        if (tags.general.length) copyText += "\n\n" + tags.general.join(", ");

                        const groupId = GM_config.get("groupId");
                        const vkApi = new VKApi(GM_config.get("botToken"));
                        const downloadFileName1 = decodeURI(downloadNode.href);

                        const now = new Date();
                        now.setHours(now.getHours() + 1000);
                        console.log(now.getTime() / 1000);

                        switch (downloadFileName1.split(".").pop().split("?")[0]) {
                            case "jpg":
                            case "png":
                            case "jpg":
                                vkApi.wallDelayedPostPhoto(groupId, downloadFileName1, copyText, sourceLink);
                                break;
                            // case "gif":
                            //   postData1.animation = downloadFileName1;
                            //   vkApi.sendAnimation(postData1);
                            //   break;
                            // case "mp4":
                            // case "avi":
                            // case "mkv":
                            // case "webm":
                            //   postData1.video = downloadFileName1;
                            //   vkApi.sendVideo(postData1);
                            //   break;
                        }
                        break;

                    case "Telegram":
                        tags = getTags(links, false, 1);

                        copyText = "";

                        if (tags.meta.find((el) => el.includes("video"))) copyText += tags.meta.find((el) => el.includes("video")) + " ";
                        if (tags.meta.find((el) => el.includes("gif"))) copyText += tags.meta.find((el) => el.includes("gif")) + " ";
                        if (tags.meta.find((el) => el.includes("animated"))) copyText += tags.meta.find((el) => el.includes("animated")) + " ";
                        if (tags.meta.find((el) => el.includes("ai_generated")) || tags.meta.find((el) => el.includes("ai_created")) || tags.meta.find((el) => el.includes("ai-created")))
                            copyText += "#ai_generated ";
                        if (tags.meta.find((el) => el.includes("realistic"))) copyText += tags.meta.find((el) => el.includes("realistic")) + " ";
                        if (tags.copyrights.length) copyText += tags.copyrights.join(" ") + "\n";
                        if (tags.characters.length) copyText += tags.characters.join(" ") + "\n";
                        if (tags.artists.length) copyText += "by " + tags.artists.join(" ");

                        if (tags.general.length) copyText += "\n\n" + tags.general.join(", ");

                        if (sourceLink) copyText += `\n\n<a href="${sourceLink}">Source</a>`;

                        const channel = GM_getValue("channelsList").find((value) => value.username === GM_getValue("selectedChannel"));
                        const bot = new TelegramBotApi(channel.bot_token);
                        // bot.getBotAdminChannels();
                        console.log(bot);
                        //return;
                        const downloadFileName = decodeURI(downloadNode.href);

                        // const now = new Date();
                        // now.setHours(now.getHours() + 1000);
                        // console.log(now.getTime() / 1000);

                        const postData = {
                            chat_id: channel.id,
                            caption: copyText,
                            parse_mode: "HTML",
                            // date: Math.floor(now.getTime() / 1000),
                        };

                        switch (downloadFileName.split(".").pop().split("?")[0]) {
                            case "jpg":
                            case "png":
                            case "jpg":
                                postData.photo = downloadFileName;
                                console.log(downloadFileName);
                                // const response = await xmlHttpRequest({
                                //   method: "GET",
                                //   url: downloadFileName,
                                //   headers: {'X-HTTP-Method-Override': 'HEAD'},
                                //   followRedirects: true
                                // });
                                // imageSize = response.getHeaders()['Content-Range'].split("/")[1];
                                // const imageSize = await fetch(downloadFileName, {
                                //   method: 'GET',
                                //   headers: {'X-HTTP-Method-Override': 'HEAD'},
                                //   followRedirects: true
                                // }).then((response) => response.getHeaders()['Content-Range'].split("/")[1]);
                                // console.log(imageSize)
                                // //console.log(imageBlob.type);
                                // //const maxSizeBytes = 50 * 1024 * 1024;
                                // if (imageSize.size >= 5) return notify("Max image size 50 MB", ["danger"]);
                                try {
                                    await bot.sendPhoto(postData);
                                } catch (error) {
                                    notify("Trying again with low resolution");
                                    let lowResDownloadNode = document.querySelector("a#lowres, img#image");
                                    let url = lowResDownloadNode.href ?? lowResDownloadNode.src;
                                    postData.photo = decodeURI(url);
                                    let response = await bot.sendPhoto(postData);
                                    if (response.ok) {
                                        removeLastNotification();
                                        notify("Success!");
                                    } else notify("Error, close window or try again", ["danger", "not_hide"]);
                                }
                                break;
                            case "gif":
                                postData.animation = downloadFileName;
                                bot.sendAnimation(postData);
                                break;
                            case "mp4":
                            case "avi":
                            case "mkv":
                            case "webm":
                                postData.video = downloadFileName.split("?")[0];
                                bot.sendVideo(postData);
                                break;
                        }

                        console.log(JSON.stringify(postData));
                        break;
                }
                //copyToClipboard(copyText);
                notify(copyText);
                modal.remove();
            });

            cancelBtn.addEventListener("click", () => {
                modal.remove();
            });

            window.addEventListener("click", (event) => {
                if (event.target === modal) {
                    modal.remove();
                }
            });
        };

        sidebarNode.before(sendToTGButton);
    }

    const TelegramBotApi = (function () {
        const _ = new Proxy(
            {},
            {
                get: (obj, name) => {
                    if (obj[name] === undefined) {
                        obj[name] = Symbol(name);
                    }
                    return obj[name];
                },
                set: () => {},
            }
        );

        let endpoint = "https://api.telegram.org/bot";

        function TelegramBotApi(token) {
            this[_.token] = token;
            endpoint += this[_.token];
        }

        const proto = TelegramBotApi.prototype;

        proto.sendPhoto = async function (data) {
            let url = endpoint + "/sendPhoto";
            const response = await this.fetchRequest("POST", url, data);
            console.log(response);
            return response;
        };

        proto.sendAnimation = async function (data) {
            let url = endpoint + "/sendAnimation";
            try {
                const response = await this.fetchRequest("POST", url, data);
                console.log(response);
            } catch (error) {
                notify(error, ["danger", "not_hide"]);
            }
        };

        proto.sendVideo = async function (data) {
            let url = endpoint + "/sendVideo";
            try {
                const response = await this.fetchRequest("POST", url, data);
                console.log(response);
            } catch (error) {
                notify(error, ["danger", "not_hide"]);
            }
        };

        proto.getChat = async function (data) {
            let url = endpoint + "/getChat";
            try {
                const response = await this.fetchRequest("POST", url, data);
                console.log(response);
                return response;
            } catch (error) {
                notify(error, ["danger", "not_hide"]);
            }
        };

        proto.getBotAdminChannels = async function () {
            let url = endpoint + "/getUpdates";
            try {
                const response = await this.fetchRequest("POST", url, {});
                console.log("Список каналов, в которых бот является администратором:", response);
            } catch (error) {
                console.error("Ошибка при получении списка каналов, в которых бот является администратором:", error);
            }
        };

        proto.fetchRequest = async function (method, url, data) {
            let headers = new Headers();
            headers.append("Content-Type", "application/json;charset=UTF-8");
            return await fetch(url, {
                method: method,
                headers: headers,
                body: JSON.stringify(data),
            }).then(async function (response) {
                if (!response.ok) {
                    notify(JSON.stringify(response), ["danger", "not_hide"]);
                    throw new Error(response.statusText);
                }
                return await response.json();
            });
            // .catch(function (error) {
            //   notify(error, ["danger", "not_hide"]);
            // });
        };

        return TelegramBotApi;
    })();

    const VKApi = (function () {
        const _ = new Proxy(
            {},
            {
                get: (obj, name) => {
                    if (obj[name] === undefined) {
                        obj[name] = Symbol(name);
                    }
                    return obj[name];
                },
                set: () => {},
            }
        );

        let endpoint = "https://api.vk.com/method/";
        const version = "5.131";

        function VKApi(accessToken) {
            this[_.accessToken] = accessToken;
        }

        const proto = VKApi.prototype;

        proto.getWallUploadServer = async function (groupId) {
            let url = endpoint + "photos.getWallUploadServer";
            let params = {
                group_id: groupId,
            };

            try {
                const response = await this.fetchRequest("GET", url, params);
                if (response && response.response) {
                    return response.response.upload_url;
                }
            } catch (error) {
                console.error(error);
                throw new Error("Failed to get wall upload server");
            }
        };

        proto.uploadPhotoToServer = async function (uploadUrl, imageBlob) {
            try {
                let file = new File([imageBlob], "img.jpg", { type: imageBlob.type, lastModified: new Date().getTime() });
                let dataTransfer = new DataTransfer();
                dataTransfer.items.add(file);

                const response = await this.sendFile(uploadUrl, dataTransfer.files[0]);
                console.log(response);

                return response;
            } catch (error) {
                console.error(error);
                throw new Error("Failed to upload photo to server");
            }
        };

        proto.sendFile = async function (uploadUrl, file) {
            const formData = new FormData();
            formData.append("photo", file);

            return new Promise(async (resolve, reject) => {
                try {
                    const response = await xmlHttpRequest({
                        method: "POST",
                        url: uploadUrl,
                        data: formData,
                        responseType: "json",
                    });
                    resolve(response);
                } catch (error) {
                    reject(error);
                }
            });
        };

        proto.saveWallPhoto = async function (groupId, server, photo, hash) {
            let url = endpoint + "photos.saveWallPhoto";
            let params = {
                server: server,
                group_id: groupId,
                photo: photo,
                hash: hash,
            };

            try {
                const response = await this.fetchRequest("POST", url, params);
                if (response && response.response) {
                    const photoData = response.response[0];
                    return {
                        owner_id: photoData.owner_id,
                        id: photoData.id,
                    };
                }
            } catch (error) {
                console.error(error);
                throw new Error("Failed to save wall photo");
            }
        };

        // proto.wallPostWithImage = async function (message, imageUrl, ownerId) {
        //   try {
        //     const uploadUrl = await this.getWallUploadServer();
        //     const serverResponse = await this.uploadPhotoToServer(uploadUrl, imageUrl);
        //     const { server, photo, hash } = serverResponse;
        //     const photoData = await this.saveWallPhoto(server, photo, hash);

        //     let url = endpoint + "wall.post";
        //     let params = {
        //       owner_id: ownerId,
        //       message: message,
        //       attachments: `photo${photoData.owner_id}_${photoData.id}`,
        //     };

        //     const response = await this.fetchRequest("POST", url, params);
        //     console.log(response);
        //   } catch (error) {
        //     console.error(error);
        //     throw new Error("Failed to post message with image");
        //   }
        // };

        proto.getLastDelayedPostTime = async function (ownerId) {
            const url = `${endpoint}wall.get`;
            const params = {
                owner_id: ownerId,
                filter: "postponed",
                count: 100,
            };

            // try {
            const response = await this.fetchRequest("GET", url, params);
            if (response.response.count > 0) {
                const itemsArray = [...response.response.items];
                if (response.response.count > 100) {
                    params.offset = 100;
                    const response2 = await this.fetchRequest("GET", url, params);
                    itemsArray.push(...response2.response.items);
                }
                const lastPostDate = itemsArray.pop().date;

                const timestamp = lastPostDate * 1000; // Предполагаем, что `unixtime` представлен в секундах

                const date = new Date(timestamp);
                const hours = date.getHours();

                if (hours >= 0 && hours <= 5) {
                    const nextMorning = new Date(date.getFullYear(), date.getMonth(), date.getDate(), 4, 0, 0);
                    return Math.floor(nextMorning.getTime() / 1000);
                }

                return lastPostDate;
            }

            //console.log(response);
            //   const { items } = response.response;
            //   if (items.length > 0) {
            //     const { date } = items[0];
            //     const lastPostTime = new Date(date * 1000); // Преобразование времени из UNIX-формата в JavaScript Date
            //     return lastPostTime;
            //   } else {
            //     console.log("Отложенных постов не найдено");
            //     return null;
            //   }
            // } catch (error) {
            //   console.error("Ошибка при получении последнего отложенного поста:", error);
            //   return null;
            // }
            // }
        };

        proto.wallPost = async function (ownerId, message, attachments = [], copyright = "", publishDate = "", fromGroup = 0, link_title = "") {
            let url = endpoint + "wall.post";
            let params = {
                owner_id: ownerId,
                message: message,
                from_group: fromGroup,
                attachments: attachments,
                copyright: copyright,
                publish_date: publishDate,
            };

            try {
                const response = await this.fetchRequest("POST", url, params);
                console.log(response);
                return response;
            } catch (error) {
                console.error(error);
            }
        };

        proto.wallDelayedPostPhoto = async function (groupId, photoUrl, text, copytight = "") {
            notify("Task started, don't close window!", ["warning", "not_hide"], "task_started");
            const imageBlob = await fetch(photoUrl).then((response) => response.blob());
            console.log(imageBlob.type);

            const maxSizeBytes = 50 * 1024 * 1024;
            if (imageBlob.size >= maxSizeBytes) return notify("Max image size 50 MB", ["danger"]);

            const uploadUrl = await this.getWallUploadServer(-groupId);
            console.log(photoUrl);

            const serverResponse = await this.uploadPhotoToServer(uploadUrl, imageBlob);
            if (serverResponse.server) notify("Uploading image to server", ["warning", "not_hide"], "upload_started");
            const { server, photo, hash } = serverResponse;
            const photoData = await this.saveWallPhoto(-groupId, server, photo, hash);

            let lastPostDate = await this.getLastDelayedPostTime(groupId);
            let postDate;
            if (!lastPostDate) {
                const time = [0, 2, 4, 6, 8, 10, 12, 14, 16 , 18, 20, 22];
                const date = new Date();
                const utc = date.getTime() + date.getTimezoneOffset() * 60 * 1000;
                const offset = 3 * 60 * 60 * 1000;
                const dateWithOffset = new Date(utc + offset);
                const currentHour = dateWithOffset.getHours();
                const currentIndex = time.indexOf(currentHour);
                if (currentIndex !== -1 && currentIndex < time.length) {
                    let index = currentIndex === time.length - 1 ? 0 : currentIndex + 1;
                    const nextHour = time[index];
                    console.log("Следующий час в массиве:", nextHour);
                    // dateWithOffset.setHours(nextHour);

                    if (date.getHours() >= nextHour) {
                        dateWithOffset.setDate(dateWithOffset.getDate() + 1);
                    }

                    dateWithOffset.setHours(nextHour, 0, 0, 0);

                    postDate = Math.floor(dateWithOffset.getTime() / 1000);
                } else {
                    console.log("Нет следующего часа в массиве.");
                }
            } else {
                postDate = lastPostDate + 2 * 60 * 60;
            }

            const pinLink = Math.floor(Math.random() * 2);
            let attachments = `photo${photoData.owner_id}_${photoData.id}`;
            if (pinLink === 1) attachments += ", https://t.me/realhentai";
            const post = await this.wallPost(groupId, text, attachments, copytight, postDate);

            removeNotification(document.querySelector("#task_started"), 0);
            removeNotification(document.querySelector("#upload_started"), 0);
            if (post.response.post_id) {
                const date = new Date(postDate * 1000);
                const hours = date.getHours().toString().padStart(2, "0");
                const minutes = date.getMinutes().toString().padStart(2, "0");
                const day = date.getDate().toString().padStart(2, "0");
                const month = (date.getMonth() + 1).toString().padStart(2, "0");
                const year = date.getFullYear();

                const formatedDate = `${hours}:${minutes} ${day}.${month}.${year}`;
                notify(`Delayed post created, ${formatedDate}`);
            } else {
                notify("Error", ["danger", "not_hide"]);
            }
        };

        proto.fetchRequest = async function (method, url, params) {
            params.v = version;
            params.access_token = this[_.accessToken];

            const queryString = Object.entries(params)
                .map(([key, value]) => `${encodeURIComponent(key)}=${encodeURIComponent(value)}`)
                .join("&");
            url += `?${queryString}`;

            const response = await xmlHttpRequest({
                method: method,
                url: url,
                data: queryString,
                responseType: "json",
            });

            console.log(response);

            if (!response.response) notify("Error", ["danger", "not_hide"]);

            return response;

            // return await fetch(url, {
            //   method: method,
            // })
            //   .then(async function (response) {
            //     if (!response.ok) {
            //       throw new Error(response.statusText);
            //     }
            //     return await response.json();
            //   })
            //   .catch(function (error) {
            //     console.error(error);
            //   });
        };

        return VKApi;
    })();

    GM_addStyle(`
    .bt-button-iqdb {
        display: inline-block;
    }

    .bt-button {
        border: none;
        box-shadow: none !important;
        display: block;
        font-family: sans-serif !important;
        font-size: 14px;
        font-weight: 300;
        color: #0a0;
        opacity: 1;
        cursor: pointer;
        padding: .3rem !important;
        text-align: center;
        background-color: #e5f6e5;
        -webkit-border-radius: 1rem;
        border-radius: 1rem;
        margin: .5rem;
    }

    .bt-button:hover {
        background-color: #0a0 !important;
        ;
        color: #fff
    }

    @-webkit-keyframes bt-notification_show {
        0% {
            -webkit-transform: translateX(-300px);
            transform: translateX(-300px)
        }
        100% {
            -webkit-transform: translateX(0);
            transform: translateX(0)
        }
    }

    @keyframes bt-notification_show {
        0% {
            -webkit-transform: translateX(-300px);
            transform: translateX(-300px)
        }
        100% {
            -webkit-transform: translateX(0);
            transform: translateX(0)
        }
    }

    @-webkit-keyframes bt-notification_hide {
        0% {
            -webkit-transform: translateX(0);
            transform: translateX(0)
        }
        100% {
            -webkit-transform: translateX(300px);
            transform: translateX(300px)
        }
    }

    @keyframes bt-notification_hide {
        0% {
            -webkit-transform: translateX(0);
            transform: translateX(0)
        }
        100% {
            -webkit-transform: translateX(-300px);
            transform: translateX(300px)
        }
    }

    .bt-notification__item.show {
        -webkit-animation: bt-notification_show .3s ease forwards;
        animation: bt-notification_show .3s ease forwards
    }

    .bt-notification__item.hide {
        -webkit-animation: bt-notification_hide .3s ease forwards;
        animation: bt-notification_hide .3s ease forwards
    }

    .bt-notification__item.danger {
        background-color: #ff4040
    }

    .bt-notification__item.warning {
        background-color: #f39c12
    }

    .bt-notification {
        width: 300px;
        background: 0 0;
        height: -webkit-fit-content;
        height: -moz-fit-content;
        height: fit-content;
        display: block;
        position: fixed;
        right: 0;
        bottom: 0;
        padding: 10px;
        z-index: 10000000;
        -webkit-box-sizing: border-box;
        box-sizing: border-box
    }

    .bt-notification__item {
        height: auto;
        display: block;
        background-color: #0a0;
        color: #fff;
        font-size: 16px;
        text-align: center;
        font-family: sans-serif !important;
        padding: .75rem .5rem;
        -webkit-box-sizing: border-box;
        box-sizing: border-box;
        margin-bottom: 10px;
        -webkit-border-radius: .5rem;
        border-radius: .5rem;
        box-sizing: border-box;
        white-space: pre-wrap;
        word-wrap: break-word
    }

    iframe[id][style*='height: 75%;'] {
        z-index: 9999000 !important;
        inset: unset !important;
        right: 0 !important;
        left: 0 !important;
        top: 0 !important;
        bottom: 0 !important;
        margin: auto !important;
        border: none !important;
        border-radius: 1rem;
        width: 50% !important;
        height: 50% !important
    }

    @media screen and (max-width: 660px) {
      iframe[id][style*='height: 75%;'] {
        width: 100%! important;
        margin: 1rem !important;
        height: 100% !important;
      }
    }

    .bt-modal,
    .bt-modal-content {
        top: 0;
        left: 0;
        position: fixed;
        min-width: 300px;
    }

    .bt-modal {
        display: block;
        z-index: 9999;
        width: 100%;
        height: 100%;
        background: rgba(0, 0, 0, .5)
    }

    .bt-modal-content {
      display: flex;
       background-color: #343434;
       bottom: 0;
       right: 0;
       margin: auto;
       padding: 2rem;
       border-radius: 5px;
       color: #fff;
       width: fit-content;
       height: fit-content;
       box-sizing: border-box;
       flex-direction: column;
       align-content: center;
       justify-content: center;
       align-items: center;
       gap: 0.5rem;
    }

    .bt-container {
      width: 100%;
      margin-top: .5rem;
    }

    .bt-modal button {
        display: inline-block;
        margin-top: 10px;
        width: 42%
    }

    .bt-select {
      width: 100%;
      /* margin: 1rem; */
    }

    .bt-button--variant-select-vk {

    }

    .bt-button--variant-select-telegram {

    }

    .bt-button--variant-select {
      border-radius: 0.1rem;
      background-color: #e5f6e5 !important;
      color: #0a0 !important;
    }

    .bt-button--variant-select:hover {
      background-color: #0a0 !important;
      color: #e5f6e5 !important;
    }

    .bt-button--variant-select-actived {
      background-color: #0a0 !important;
      color: #e5f6e5 !important;
    }

    .bt-button--variant-select-actived:hover {
      background-color: #0a0 !important;
      color: #e5f6e5;
    }
    `);

    let notificationContainer = document.createElement("div");
    notificationContainer.className = "bt-notification";
    document.body.appendChild(notificationContainer);

    function nextTextNodes(node) {
        let all = [];
        for (node = node.nextSibling; node; node = node.nextSibling) {
            if (node.nodeType == 3 || node.nodeName == "A") all.push(node);
        }
        return all;
    }

    function underTextNodes(node) {
        let n,
            all = [],
            walk = document.createTreeWalker(node, NodeFilter.SHOW_TEXT, null, false);
        while ((n = walk.nextNode())) all.push(n);
        return all;
    }

    function uniq(i) {
        let seen = {};
        return i.filter(function (item) {
            return seen.hasOwnProperty(item) ? false : (seen[item] = true);
        });
    }

    function copyToClipboard(text) {
        navigator.clipboard.writeText(text).then(
            function () {
                console.log("Async: Copying to clipboard was successful!");
            },
            function (error) {
                console.error("Async: Could not copy text: ", error);
            }
        );
    }

    function notify(text, customClass = null, customID = null, onCLickEvent = null, onCreatedEvent = null) {
        let notificationItem = document.createElement("div");
        notificationItem.className = "bt-notification__item";
        notificationItem.classList.add("show");
        if (customClass) notificationItem.classList.add(...customClass);
        if (customID) notificationItem.id = customID;
        notificationItem.innerHTML = text;
        notificationItem.onclick = () => {
            if (typeof onCLickEvent == "function") onCLickEvent();
            notificationItem.classList.replace("show", "hide");
            setTimeout(() => notificationItem.remove(), 300);
        };
        document.querySelector(".bt-notification").prepend(notificationItem);

        if (typeof onCreatedEvent == "function") onCreatedEvent();

        removeLastNotification();
    }

    let timer;

    function removeLastNotification(time = 3000) {
        const notifications = document.querySelectorAll(".bt-notification .bt-notification__item:not(.not_hide)");
        const lastItem = notifications[notifications.length - 1];

        clearTimeout(timer);
        if (lastItem) {
            timer = setTimeout(() => {
                lastItem.classList.replace("show", "hide");
                setTimeout(() => lastItem.remove(), 300);
                removeLastNotification(300);
            }, time);
        }
    }

    function removeNotification(notification, time = 0) {
        setTimeout(() => {
            notification.classList.replace("show", "hide");
            setTimeout(() => notification.remove(), 300);
        }, time);
    }

    function xmlHttpRequest(args) {
        return new Promise((resolve, reject) => {
            GM.xmlHttpRequest(
                Object.assign(
                    {
                        method: args.method ?? "GET",
                    },
                    args.url ? args : { url: args },
                    {
                        onload: (r) => resolve(r.response),
                        onerror: reject,
                        ontimeout: reject,
                    }
                )
            );
        });
    }

    function getFilesize(url, callback) {
        let xhr = new XMLHttpRequest();
        xhr.open("HEAD", url, true);
        xhr.onreadystatechange = function () {
            if (this.readyState == this.DONE) {
                callback(parseInt(xhr.getResponseHeader("Content-Length")));
            }
        };
        xhr.send();
    }

    function createButton(label, action) {
        let btn = document.createElement("div");
        btn.className = "bt-button";
        btn.innerHTML = label;
        btn.onclick = action;
        return btn;
    }
})();

